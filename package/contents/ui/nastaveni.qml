import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls 1 as StarodavnyQtControls
import QtQuick.Dialogs 1.0
import QtQuick.Layouts 1.12
import org.kde.kirigami 2.4 as Kirigami

Kirigami.FormLayout
{
    id: page
    Kirigami.FormData.label: i18n('Nastavení')
    
    // proměný musej mit ten prefix 'cfg_' aby si to jakože spárovalo s vodpovídajícíma hodnotama v souboru config.xml 
    property alias cfg_druhMapky: konfiguraceDruhMapky.currentIndex
    property alias cfg_rychlostAnimace: konfiguraceRychlostAnimace.value
    property alias cfg_refreshInterval: konfiguraceRefreshInterval.value
    property alias cfg_ukazovatGui: konfiguraceUkazovatGui.checked
    property alias cfg_ukazovatDatum: konfiguraceUkazovatDatum.checked
    
    ComboBox
    {
        id: konfiguraceDruhMapky
        model: ['Déšť', 'Teplota', 'Vítr', 'Mraky', 'Ventilační index', 'Relativní vlhkost']
        Kirigami.FormData.label: i18n('druh mapky:')
        onActivated:
        {
            console.log(konfiguraceDruhMapky.currentIndex);
        }
    }

        
    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceRychlostAnimace
        decimals: 2
        stepSize: 0.1
        minimumValue: 0.1
        suffix: 'x'
        Kirigami.FormData.label: i18n('rychlost přehrávání animace:')
    }

    StarodavnyQtControls.SpinBox
    {
        id: konfiguraceRefreshInterval
        decimals: 0
        minimumValue: 1
        suffix: ' minut'
        Kirigami.FormData.label: i18n('refreshovávací interval:')
    }
    
    CheckBox
    {
        id: konfiguraceUkazovatGui
        Kirigami.FormData.label: i18n('kreslit gui:')
    }
    
    CheckBox
    {
        id: konfiguraceUkazovatDatum
        Kirigami.FormData.label: i18n('vykreslovat datum:')
    }
    
}
 
