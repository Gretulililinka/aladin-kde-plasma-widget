import QtQuick 2.11
import QtQuick.Window 2.2
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.11

import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.components 3.0 as PlasmaComponents


Item
{
    
    Plasmoid.fullRepresentation: Item
    {
        id: main
        
        property int druhMapky: plasmoid.configuration.druhMapky
        property int refreshInterval: plasmoid.configuration.refreshInterval
        property real rychlostAnimace: plasmoid.configuration.rychlostAnimace
        property bool ukazovatGui: plasmoid.configuration.ukazovatGui
        property bool ukazovatDatum: plasmoid.configuration.ukazovatDatum
        
        property var obrazky: []
        property int index: 0 //index právě zobrazenýho obrázku
        
        Layout.preferredWidth: 600 * PlasmaCore.Units.devicePixelRatio
        Layout.preferredHeight: (362 + (ukazovatGui?36:0)) * PlasmaCore.Units.devicePixelRatio

        
        // sem budou nastrkaný vobrázky animace
        Rectangle
        {
            id: kontik
            width: parent.width
            height: ukazovatGui ? parent.height - prehravaciGui.height - 10 : parent.height
            color: '#00000000'
        }
        
        Row
        {
            id: prehravaciGui
            width: parent.width
            height: PlasmaCore.Units.devicePixelRatio * 36 - 5
            y: parent.height - height
            x: parent.width/7
            opacity: 0.75
            visible: ukazovatGui

            
            Button
            {
                width: main.width / 7
                height: parent.height
                contentItem: PlasmaCore.IconItem
                {
                    source: "media-skip-backward"
                }
                padding: 0
                onClicked:
                {
                    firstObrazek();
                    timerAnimace.restart();
                }
            }
            Button
            {
                width: main.width / 7
                height: parent.height
                contentItem: PlasmaCore.IconItem
                {
                    source: "media-seek-backward"
                }
                padding: 0
                onClicked:
                {
                    prevObrazek();
                    timerAnimace.restart();
                }
            }
            Button
            {
                width: main.width / 7
                height: parent.height
                id: playButton
                contentItem: PlasmaCore.IconItem
                {
                    source: timerAnimace.running ? "media-playback-pause" : "media-playback-start"
                }
                padding: 0
                onClicked:
                {
                    if(timerAnimace.running)
                    {
                        timerAnimace.stop()
                    }
                    else
                    {
                        timerAnimace.start();
                    }
                }
            }
            Button
            {
                width: main.width / 7
                height: parent.height
                contentItem: PlasmaCore.IconItem
                {
                    source: "media-seek-forward"
                }
                onClicked:
                {
                    nextObrazek();
                    timerAnimace.restart();
                }
                padding: 0
            }
            Button
            {
                width: main.width / 7
                height: parent.height
                contentItem: PlasmaCore.IconItem
                {
                    source: "media-skip-forward"
                }
                onClicked:
                {
                    lastObrazek();
                    timerAnimace.restart();
                }
                padding: 0
            }
                
        }

        
        // časovač na přehrávání animace
        // ne všechny obrázky sou stažený a ne všecky de postahovat
        // když třeba teprve počitaj modýlek tak tam ty vobrázky eště nejsou na stažení
        // a qml image element kterej bude mit takovej obrázek jako source nebude možný zobrazit tak ho
        // jakoby musíme přeskočit
        Timer
        {
            id: timerAnimace
            interval: 1000 * (1.0/rychlostAnimace)
            repeat: true
            running: true
            onTriggered: nextObrazek()
        }
        
        // časovač na stahobání obrázků
        Timer
        {
            interval: 1000 * 60 * refreshInterval
            repeat: true
            running: true
            onTriggered: vyrobitObrazky()
        }
        
        Component.onCompleted: vyrobitObrazky()
        
        onDruhMapkyChanged: vyrobitObrazky()


        // funkce která naplní kontik vybranejma vobrázkama
        // vyrábí je jako dynamický voběkty Image který maj ten danej vobrázek jako url co vede na chmi.cz
        function vyrobitObrazky()
        {
            console.log('chmi.cz widget: stahujou se vobrázky');
            var tedko = new Date();
            tedko.setHours(0);
            var date = new Date();
            // předpověď se tam voběvuje jenom v 00 06 12 18 UTC hodin
            // takže zarovnáme na nejbližšší takovou existujicí hodinu
            var hodiny = date.getUTCHours();
            var hodina = hodiny - (hodiny % 6);
            date.setUTCHours(hodina); //setHours si samo umí v date oběktu posouvat měsíc/rok/etc dopředu/dozadu :O :O
            
            if(obrazky.length > 0)
            {
                for(let i=0;i<obrazky.length;i++)
                {
                    obrazky[i].destroy();
                }
                //obrazky.forEach(destroy);
                obrazky = [];
            }
            
            
            for(let i=0;i<10;i++)
            {
                // obrázky skovávaj do složšky která se menuje podle aktuálního UTC datumu 
                var url = date.getUTCFullYear() + ("0"+(date.getUTCMonth()+1)).slice(-2) + ("0"+(date.getUTCDate())).slice(-2) + ("0"+(date.getUTCHours())).slice(-2);
                
                url = 'https://www.chmi.cz/files/portal/docs/meteo/ov/aladin/results/public/mapy/data/' + url;
                
                // kouknem se requestem jestli tamta složška už existuje
                // jestli ne tak předpověď pro todlecto datum eště neni hotová noa jestli jo tak už
                // je a mužem si asi jakoby začít stahovat ty vobrázky jejich
                let xmlHttpReq = new XMLHttpRequest();
                xmlHttpReq.open("GET", url, false);
                xmlHttpReq.send();
                
                // status kód to furt vrací 0 (asi kuli cross origin)
                // kódu 403 kterej nám řiká že stránka existuje jen se tam nesmí ale vodpovídá responseText vo dýlce 162 znaků
                // pro 404 to vrací něco vo moc delšího
                if(xmlHttpReq.responseText.length == 162)
                {
                    //stránka existuje
                    
                    // prefixy jednotlivejch vobrázků podle druhů předpovědí
                    var prefix = 'prec_public_rd_';
                    switch(druhMapky)
                    {
                        case 0:
                            prefix = 'prec_public_rd_'; // déšť
                            // to předpovídání pršení maj dycky jakoby na příští 3 hodiny
                            // (v porovnání s vostatníma udajema jim první vobrázek jakoby chybí)
                            date.setHours(date.getHours()+3);
                            break;
                        case 1:
                            prefix = 'T2m_public_'; // teplota
                            break;
                        case 2:
                            prefix = 'v10mmslp_public_'; // foukání větru v 10 metrech nad zemí
                            break;
                        case 3:
                            prefix = 'nebul_public_'; // mraky
                            break;
                        case 4:
                            prefix = 'veind_public_'; // ventilační index
                            break;
                        case 5:
                            prefix = 'RH2m_public_'; // relativní vlchkost
                            break;
                        default:
                            break;
                    }
                    
                    // obrázků maj dycky maximalně 24
                    // vyjímku má jenom předpověď pršení ta má jenom 23 a začíná
                    // dycky až za 3 hodiny v budoucnosti vod předpovědi :O :O
                    for(let j=(druhMapky==0?1:0);j<=24;j++)
                    {
                    
                        let obrazek_url = url + '/' + prefix + ("0"+(j*3)).slice(-2) + '.png';
                        
                        // porovnáme si posunutej čas s aktuálním a podle toho poznáme na kterej den to máme předpověď
                        // (to se pak strčí jako text do takovýho malýho bílího čtverečku podobně jako to maj voni na stránkách)
                        var den ='';
                        var delta = Math.floor((date - tedko) / (1000*60*60*24))
                        switch(delta)
                        {
                            case -3:den = 'předpředevčírem';break;
                            case -2:den = 'předevčírem';break;
                            case -1:den = 'včera';break;
                            case 0:den = 'dneska';break;
                            case 1:den = 'zejtra';break;
                            case 2:den = 'pozejtří';break;
                            case 3:den = 'popozejtří';break;
                            default:den = 'někdy :D';break; 
                        }
                        
                        den += ' ' + ("0"+(date.getHours())).slice(-2) + ':00'
                        date.setHours(date.getHours()+3);
                        
                        // vyrobíme si qml element jako dynamickej voběkt
                        // je to obrázek kterej má vsobě strčenej element Rectangle kterej má v sobě strčenej textik s datumem
                        // všecky vobrázky sou vyrobený jako neviditelný a animace funguje tak že timer dycky jeden zobrazí a sková předchozí
                        // (nastrkat to do animatedImage to asi jenom ve vobyč qml bez c++ nejde :O :O)
                        var obektString = 'import QtQuick 2.11;';
                        obektString += 'Image{';
                        obektString += 'fillMode: Image.PreserveAspectFit;';
                        obektString += 'anchors.fill: parent;';
                        obektString += 'visible: false;';
                        obektString += 'source: \"' + obrazek_url +'\";';
                        
                        
                        obektString += 'Rectangle{Text{';
                        obektString += 'text: \"'+den+'\";';
                        obektString += 'width: parent.width*0.8;height: parent.height*0.8;';
                        obektString += 'id:txt;';
                        obektString += 'y:-2;';
                        obektString += 'anchors.centerIn: parent;font.pointSize: 256;minimumPointSize: 8;fontSizeMode: Text.Fit;';
                        
                        obektString += '}';
                        obektString += 'color:\'white\';';
                        obektString += 'x: parent.paintedWidth * 0.005 + (parent.width - parent.paintedWidth)/2;';
                        obektString += 'y: parent.paintedHeight * 0.0085 + (parent.height - parent.paintedHeight)/2;';
                        obektString += 'width: Math.max(parent.paintedWidth * 0.2,100);';
                        obektString += 'height: Math.max(parent.paintedHeight * 0.05,15);';
                        obektString += 'visible: ukazovatDatum && (width < main.width);';
                        obektString += 'id: rect;';                        
                        
                        obektString += '}';
                        obektString += '}';
                        
                        var obekt = Qt.createQmlObject(obektString, kontik , 'obrazek_'+j);
                        obrazky.push(obekt);
                    }
                    
                    // pokud to pude zkusíme zobrazit první vobrázek
                    if(obrazky[0].status == Image.Ready)
                        obrazky[0].visible=true;
                    
                    return true;
                    
                }
                else
                {
                    //předpověď eště neexistuje -> vodeberem z datumu 6 hodin a zkusíme znova
                    console.log('posouvam cas vo 6h dozadu');
                    date.setUTCHours(hodina-6);
                    continue;
                }

            }
            return false;
        }
        
        function nextObrazek()
        {
            if (obrazky.length > 0)
            {
                if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)
                    obrazky[index].visible = false;
                        
                for(let i = 0; i < obrazky.length; i++)
                {
                    index++;
                    if(index == obrazky.length)
                    {
                        index = 0;
                    }
                            
                    // ukazujem jenom obrázky který sou ready -> tzn. už stažený a připravený
                    if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)
                    {
                        obrazky[index].visible = true;
                        break;
                    }

                }
            }
        }
        
        function prevObrazek()
        {
            if (obrazky.length > 0)
            {
                if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)    
                    obrazky[index].visible = false;
                        
                for(let i = 0; i < obrazky.length; i++)
                {
                    index--;
                    if(index < 0)
                    {
                        index = obrazky.length - 1;
                    }
                            
                    // ukazujem jenom obrázky který sou ready -> tzn. už stažený a připravený
                    if(obrazky[index] !== undefined &&  obrazky[index].status == Image.Ready)
                    {
                        obrazky[index].visible = true;
                        break;
                    }

                }
            }
        }
        
        function firstObrazek()
        {
            if (obrazky.length > 0)
            {
                if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)    
                    obrazky[index].visible = false;
                
                index = 0;
                for(let i = 0; i < obrazky.length; i++)
                {
                            
                    // ukazujem jenom obrázky který sou ready -> tzn. už stažený a připravený
                    if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)
                    {
                        obrazky[index].visible = true;
                        break;
                    }
                    else
                    {
                        index++;
                    }

                }
            }
        }
        
        
        function lastObrazek()
        {
            if (obrazky.length > 0)
            {
                if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)
                    obrazky[index].visible = false;
                
                index = obrazky.length - 1;
                for(let i = 0; i < obrazky.length; i++)
                {
                            
                    // ukazujem jenom obrázky který sou ready -> tzn. už stažený a připravený
                    if(obrazky[index] !== undefined && obrazky[index].status == Image.Ready)
                    {
                        obrazky[index].visible = true;
                        break;
                    }
                    else
                    {
                        index--;
                    }

                }
            }
        }
    }
}
